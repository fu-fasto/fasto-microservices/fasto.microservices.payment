﻿using System;

namespace FastO.Microservices.Payment.Dtos
{
    public class CreatePaymentTransactionResponse
    {
        public Guid Id { get; set; }

        public dynamic TransactionData { get; set; }
    }
}