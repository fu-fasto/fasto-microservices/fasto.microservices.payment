﻿using System;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Payment.Queries
{
    public class GetAllPaymentAccounts : AllQueryBase<PaymentAccount>
    {
        public Guid? StoreId { get; set; }
    }
}