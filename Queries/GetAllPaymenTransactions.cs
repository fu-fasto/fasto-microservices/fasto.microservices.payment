﻿using System;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Payment.Queries
{
    public class GetAllPaymenTransactions : AllQueryBase<PaymentTransaction>
    {
        public Guid? StoreId { get; set; }

        public Guid? CustomerId { get; set; }

        public PaymentState? State { get; set; }
    }
}