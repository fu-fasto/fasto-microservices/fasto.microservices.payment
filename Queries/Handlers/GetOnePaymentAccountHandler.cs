﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Payment.Queries.Handlers
{
    public class GetOnePaymentAccountHandler : IQueryHandler<GetOnePaymentAccount, PaymentAccount>
    {
        private readonly IRepository<PaymentAccount,Guid> _repository;

        public GetOnePaymentAccountHandler(IRepository<PaymentAccount, Guid> repository)
        {
            _repository = repository;
        }
        
        public async Task<PaymentAccount> Handle(GetOnePaymentAccount request, CancellationToken cancellationToken)
        {
            var paymentAccount = await _repository.FindOneAsync(request.Id, cancellationToken) ??
                                 throw new NullReferenceException(
                                     $"Payment Account with id={request.Id} does not exist");
            return paymentAccount;
        }
    }
}