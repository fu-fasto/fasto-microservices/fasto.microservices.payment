﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Payment.Queries.Handlers
{
    public class GetAllPaymentAccountsHandler : IQueryHandler<GetAllPaymentAccounts, IEnumerable<PaymentAccount>>
    {
        private readonly IRepository<PaymentAccount, Guid> _repository;

        public GetAllPaymentAccountsHandler(IRepository<PaymentAccount, Guid> repository)
        {
            _repository = repository;
        }
        
        public async Task<IEnumerable<PaymentAccount>> Handle(GetAllPaymentAccounts request, CancellationToken cancellationToken)
        {
            var findQuery = request.MapTo<FindQuery<PaymentAccount>>();

            if (request.StoreId.HasValue)
            {
                findQuery.Filters.Add(x => x.StoreId.Equals(request.StoreId.Value));
            }

            return await _repository.FindAsync(findQuery, cancellationToken);
        }
    }
}