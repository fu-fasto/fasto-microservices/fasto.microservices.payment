﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Payment.Queries.Handlers
{
    public class GetAllPaymentTransactionsHandler : IQueryHandler<GetAllPaymenTransactions, IEnumerable<PaymentTransaction>>
    {
        private readonly IRepository<PaymentTransaction, Guid> _repository;

        public GetAllPaymentTransactionsHandler(IRepository<PaymentTransaction, Guid> repository)
        {
            _repository = repository;
        }
        
        public async Task<IEnumerable<PaymentTransaction>> Handle(GetAllPaymenTransactions query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<PaymentTransaction>>();
            if (query.StoreId.HasValue)
                findQuery.Filters.Add(x => x.StoreId.Equals(query.StoreId));
            if (query.CustomerId.HasValue)
                findQuery.Filters.Add(x => x.CustomerId.Equals(query.CustomerId));
            if (query.State.HasValue)
                findQuery.Filters.Add(x => x.State.Equals( PaymentState.Paid));
            return await _repository.FindAsync(findQuery, cancellationToken);
        }
    }
}