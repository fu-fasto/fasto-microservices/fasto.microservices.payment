﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Payment.Queries.Handlers
{
    public class GetOnePaymentTransactionHandler : IQueryHandler<GetOnePaymentTransaction, PaymentTransaction>
    {
        private readonly IRepository<PaymentTransaction, Guid> _repository;

        public GetOnePaymentTransactionHandler(IRepository<PaymentTransaction, Guid> repository)
        {
            _repository = repository;
        }
        
        public async Task<PaymentTransaction> Handle(GetOnePaymentTransaction request, CancellationToken cancellationToken)
        {
            var paymentTransaction = await _repository.FindOneAsync(request.Id, cancellationToken) ??
                                     throw new NullReferenceException(
                                         $"Payment Transaction with id={request.Id} does not exist");
            return paymentTransaction;
        }
    }
}