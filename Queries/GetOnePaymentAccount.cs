﻿using System;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Payment.Queries
{
    public class GetOnePaymentAccount : OneQueryBase<PaymentAccount, Guid>
    {
        
    }
}