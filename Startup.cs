using FastO.Microservices.Payment.DataModels;
using FastO.Microservices.Payment.Services;
using FastO.Microservices.Payment.Services.Momo;
using MicroBoost;
using MicroBoost.Cqrs;
using MicroBoost.Jaeger;
using MicroBoost.MessageBroker;
using MicroBoost.Metrics;
using MicroBoost.Persistence;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace FastO.Microservices.Payment
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMicroBoostBuilder()
                .AddJaeger()
                .AddWebApi()
                .AddSwaggerDocs()
                .AddCqrs()
                .AddSqlPersistence<PaymentDbContext>()
                .AddMessageBroker()
                .AddSingleton<IPaymentService, PaymentService>()
                .AddSingleton<MomoService>()
                .RegisterOptions<PaymentConfiguration>("PaymentConfiguration")
                .Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app
                .UseMetrics()
                .UseJaeger()
                .UseWebApi()
                .UseSwaggerDocs();
        }
    }
}