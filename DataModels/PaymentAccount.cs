﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Payment.DataModels
{
    public class PaymentAccount : EntityBase<Guid>
    {
        /// <summary>
        /// Id of payment account
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Id of store
        /// </summary>
        public Guid StoreId { get; set; }

        /// <summary>
        /// Payment provider
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Additional data
        /// </summary>
        public string AccountData { get; set; }

        /// <summary>
        /// Activated state
        /// </summary>
        public bool IsActivated { get; set; }
    }

    public static class Provider
    {
        public const string PayPal = "PAYPAL";
        public const string Momo = "MOMO";
        public const string ZaloPay = "ZALOPAY";
        public const string AirPay = "AIRPAY";
    }
}