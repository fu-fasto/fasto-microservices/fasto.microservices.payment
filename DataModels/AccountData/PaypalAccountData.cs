﻿namespace FastO.Microservices.Payment.DataModels.AccountData
{
    public class PaypalAccountData
    {
        public string Email { get; set; }
    }
}