﻿using MicroBoost.Persistence.EfCore;
using Microsoft.EntityFrameworkCore;

namespace FastO.Microservices.Payment.DataModels
{
    public sealed class PaymentDbContext : EfCoreContext
    {
        public DbSet<PaymentAccount> PaymentAccounts { get; set; }
        public DbSet<PaymentTransaction> PaymentTransactions { get; set; }

        public PaymentDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PaymentAccount>().ToTable("payment_accounts");
            modelBuilder.Entity<PaymentAccount>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<PaymentAccount>().Property(x => x.StoreId).HasColumnName("store_id");
            modelBuilder.Entity<PaymentAccount>().Property(x => x.IsActivated).HasColumnName("is_activated");
            modelBuilder.Entity<PaymentAccount>().Property(x => x.Provider).HasColumnName("provider").HasMaxLength(50).IsRequired();
            modelBuilder.Entity<PaymentAccount>().Property(x => x.AccountData).HasColumnName("account_data");

            modelBuilder.Entity<PaymentTransaction>().ToTable("payment_transactions");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.StoreId).HasColumnName("store_id");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.CustomerId).HasColumnName("customer_id");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.StoreName).HasColumnName("store_name");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.CustomerName).HasColumnName("customer_name");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.Provider).HasColumnName("provider").HasMaxLength(50).IsRequired();
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.State).HasColumnName("state");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.CreateAt).HasColumnName("create_at").ValueGeneratedOnAdd();
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.CompletedAt).HasColumnName("complete_at").ValueGeneratedOnUpdate();
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.Price).HasColumnName("price");
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.Currency).HasColumnName("currency").HasMaxLength(3);
            modelBuilder.Entity<PaymentTransaction>().Property(x => x.TransactionData).HasColumnName("transaction_data");
        }
    }
}