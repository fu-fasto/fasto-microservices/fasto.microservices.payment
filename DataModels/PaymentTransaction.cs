﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Payment.DataModels
{
    public class PaymentTransaction : EntityBase<Guid>
    {
        /// <summary>
        /// Id of payment transaction
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Id of store
        /// </summary>
        public Guid StoreId { get; set; }

        /// <summary>
        /// Id of customer
        /// </summary>
        public Guid CustomerId { get; set; }
        
        /// <summary>
        /// Name of store
        /// </summary>
        public string StoreName { get; set; }
        
        /// <summary>
        /// Name of customer
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Payment provider
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Current payment state
        /// </summary>
        public PaymentState State { get; set; }

        /// <summary>
        /// Created time
        /// </summary>
        public DateTimeOffset CreateAt { get; set; }

        /// <summary>
        /// Completed time
        /// </summary>
        public DateTimeOffset CompletedAt { get; set; }

        /// <summary>
        /// Price of transaction
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Currency of transaction
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Additional data
        /// </summary>
        public string TransactionData { get; set; }
    }

    public enum PaymentState
    {
        Created = 0,
        Paid = 1
    }
}