﻿namespace FastO.Microservices.Payment.DataModels.TransactionData
{
    public class PaypalTransactionData : TransactionData
    {
        public string PaypalOrderId { get; set; }
    }
}