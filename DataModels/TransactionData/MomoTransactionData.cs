﻿namespace FastO.Microservices.Payment.DataModels.TransactionData
{
    public class MomoTransactionData : TransactionData
    {
        public string MomoOrderId { get; set; }
        public string PayUrl { get; set; }
    }
}