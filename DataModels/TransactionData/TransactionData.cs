﻿using System;

namespace FastO.Microservices.Payment.DataModels.TransactionData
{
    public class TransactionData
    {
        public Guid VisitId { get; set; }
    }
}