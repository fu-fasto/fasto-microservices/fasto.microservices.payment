﻿using System;
using FastO.Microservices.Payment.DataModels.TransactionData;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Payment.Commands
{
    public class CreatePaymentTransaction  : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public string Provider { get; set; }
        
        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public Guid VisitId { get; set; }
        
        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }

        public DateTimeOffset CreateAt { get; set; } = DateTimeOffset.UtcNow;

        public decimal Price { get; set; }

        public string Currency { get; set; }
        
        public TransactionData TransactionData { get; set; }
    }
}