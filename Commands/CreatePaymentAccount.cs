﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Payment.Commands
{
    public class CreatePaymentAccount : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public Guid StoreId { get; set; }

        public string Provider { get; set; }

        public string AccountData { get; set; }
        
        public bool IsActivated { get; set; }
    }
}