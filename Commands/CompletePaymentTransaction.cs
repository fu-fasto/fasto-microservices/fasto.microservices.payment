﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Payment.Commands
{
    public class CompletePaymentTransaction : CommandBase
    {
        [MessageKey] public Guid PaymentTransactionId { get; set; } 
    }
}