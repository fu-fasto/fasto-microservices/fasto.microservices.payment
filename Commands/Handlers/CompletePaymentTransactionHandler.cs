﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using FastO.Microservices.Payment.DataModels.TransactionData;
using FastO.Microservices.Payment.Events;
using FastO.Microservices.Payment.Services.Paypal;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace FastO.Microservices.Payment.Commands.Handlers
{
    public class CompletePaymentTransactionHandler : ICommandHandler<CompletePaymentTransaction>
    {
        private readonly IRepository<PaymentTransaction, Guid> _repository;
        private readonly ILogger<CompletePaymentTransactionHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IUnitOfWork _unitOfWork;
        private readonly PaymentDbContext _dbContext;

        public CompletePaymentTransactionHandler(IRepository<PaymentTransaction, Guid> repository,
            ILogger<CompletePaymentTransactionHandler> logger, IMessagePublisher messagePublisher,
            IUnitOfWork unitOfWork, PaymentDbContext dbContext)
        {
            _repository = repository;
            _logger = logger;
            _messagePublisher = messagePublisher;
            _unitOfWork = unitOfWork;
            _dbContext = dbContext;
        }

        public async Task PreHandle(CompletePaymentTransaction command, CancellationToken cancellationToken)
        {
            var paymentTransaction = await _repository.FindOneAsync(command.PaymentTransactionId, cancellationToken) ??
                                     throw new NullReferenceException(
                                         $"Payment transaction {command.PaymentTransactionId} is not existed!");

            if (paymentTransaction.Provider.ToUpper().Equals("PAYPAL"))
            {
                var paypalTransactionData =
                    JsonSerializer.Deserialize<PaypalTransactionData>(paymentTransaction.TransactionData);
                var orderId = paypalTransactionData!.PaypalOrderId;
                await PaypalService.CaptureOrder(orderId);
            }
        }

        public async Task Handle(CompletePaymentTransaction command, CancellationToken cancellationToken)
        {
            var strategy = _dbContext.Database.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);

                try
                {
                    var paymentTransaction =
                        await _repository.FindOneAsync(command.PaymentTransactionId, cancellationToken);
                    paymentTransaction.State = PaymentState.Paid;
                    paymentTransaction.CompletedAt = DateTimeOffset.UtcNow;

                    var @event = paymentTransaction.MapTo<PaymentTransactionCompleted>();
                    @event.PaymentTransactionId = paymentTransaction.Id;
                    @event.VisitId = JsonConvert.DeserializeObject<TransactionData>(paymentTransaction.TransactionData)
                        .VisitId;

                    await _repository.UpdateAsync(paymentTransaction, cancellationToken);
                    await _messagePublisher.PublishOutBoxAsync(@event, cancellationToken);

                    _logger.LogInformation("Complete payment transaction {Id}", paymentTransaction.Id);

                    await transaction.CommitAsync(cancellationToken);
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync(cancellationToken);
                    throw;
                }
            });

            
        }
    }
}