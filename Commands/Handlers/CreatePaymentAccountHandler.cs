﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Payment.Commands.Handlers
{
    public class CreatePaymentAccountHandler : ICommandHandler<CreatePaymentAccount>
    {
        private readonly IRepository<PaymentAccount, Guid> _repository;
        private readonly ILogger<CreatePaymentAccountHandler> _logger;

        public CreatePaymentAccountHandler(IRepository<PaymentAccount, Guid> repository, ILogger<CreatePaymentAccountHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        
        public async Task Handle(CreatePaymentAccount command, CancellationToken cancellationToken)
        {
            var paymentAccount = command.MapTo<PaymentAccount>();
            await _repository.AddAsync(paymentAccount, cancellationToken);
            _logger.LogInformation("Create Payment Account with id={Id}", paymentAccount.Id);
        }
    }
}