﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Payment.Commands.Handlers
{
    public class UpdatePaymentAccountHandler : ICommandHandler<UpdatePaymentAccount>
    {
        private readonly IRepository<PaymentAccount, Guid> _repository;
        private readonly ILogger<UpdatePaymentAccountHandler> _logger;

        public UpdatePaymentAccountHandler(IRepository<PaymentAccount,Guid> repository, ILogger<UpdatePaymentAccountHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        
        public async Task Handle(UpdatePaymentAccount command, CancellationToken cancellationToken)
        {
            var paymentAccount = await _repository.FindOneAsync(command.Id, cancellationToken);
            paymentAccount.AccountData = command.AccountData;
            paymentAccount.IsActivated = command.IsActivated;
            await _repository.UpdateAsync(paymentAccount, cancellationToken);
            _logger.LogInformation("Update payment account with id={Id}", paymentAccount.Id);
        }
    }
}