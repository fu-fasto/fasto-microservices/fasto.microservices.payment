﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.DataModels;
using FastO.Microservices.Payment.Services;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FastO.Microservices.Payment.Commands.Handlers
{
    public class CreatePaymentTransactionHandler : ICommandHandler<CreatePaymentTransaction>
    {
        private readonly IRepository<PaymentTransaction, Guid> _repository;
        private readonly ILogger<CreatePaymentTransactionHandler> _logger;
        private readonly IPaymentService _paymentService;

        public CreatePaymentTransactionHandler(IRepository<PaymentTransaction, Guid> repository,
            ILogger<CreatePaymentTransactionHandler> logger, IPaymentService paymentService)
        {
            _repository = repository;
            _logger = logger;
            _paymentService = paymentService;
        }

        public async Task PreHandle(CreatePaymentTransaction command, CancellationToken cancellationToken)
        {
            await _paymentService.CreatePaymentTransaction(command, cancellationToken);
            command.TransactionData.VisitId = command.VisitId;
        }

        public async Task Handle(CreatePaymentTransaction command, CancellationToken cancellationToken)
        {
            var paymentTransaction = command.MapTo<PaymentTransaction>();
            paymentTransaction.State = PaymentState.Created;
            paymentTransaction.TransactionData = JsonConvert.SerializeObject(command.TransactionData);
            await _repository.AddAsync(paymentTransaction, cancellationToken);
            _logger.LogInformation("Create payment transaction with id={Id}", paymentTransaction.Id);
        }
    }
}