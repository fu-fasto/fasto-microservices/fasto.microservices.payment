﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Payment.Commands
{
    public class UpdatePaymentAccount : CommandBase
    {
        [MessageKey] public Guid Id { get; set; }

        public string AccountData { get; set; }

        public bool IsActivated { get; set; }
    }
}