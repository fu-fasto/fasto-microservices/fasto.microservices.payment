﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;

namespace FastO.Microservices.Payment.Services.Paypal
{
    public class PaypalService
    {

        public static OrderRequest BuildOrderRequest(CreatePaymentTransaction createPaymentTransaction)
        {
            //Hard-coded store email. Later can be changed to createPaymentTransaction.StoreEmail
            string storeEmail = "sb-57hke5157327@personal.example.com";
            
            var request = new OrderRequest()
            {
                CheckoutPaymentIntent = "CAPTURE",
                ApplicationContext = new ApplicationContext()
                {
                    BrandName = "",
                    LandingPage = "BILLING",
                    UserAction = "PAY_NOW",
                    ShippingPreference = "NO_SHIPPING",
                    PaymentMethod = new PaymentMethod()
                    {
                        PayeePreferred = "IMMEDIATE_PAYMENT_REQUIRED"
                    }
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                    new()
                    {
                        Payee = new Payee()
                        {
                            Email = storeEmail
                        },
                        AmountWithBreakdown = new AmountWithBreakdown()
                        {
                            CurrencyCode = createPaymentTransaction.Currency,
                            Value = createPaymentTransaction.Price.ToString("0.00")
                        }
                    }
                }
            };
            return request;
        }
        
        public static async Task<HttpResponse> CreateOrder(OrderRequest orderRequest, bool fullResponse = true)
        {
            var request = new OrdersCreateRequest();
            request.Prefer(fullResponse ? "return=representation" : "return=minimal");
            request.RequestBody(orderRequest);
            return await PaypalClient.GetClient().Execute(request);
        }
        
        public static async Task CaptureOrder(string orderId)
        {
            var request = new OrdersCaptureRequest(orderId);
            //request.Headers.Add("PayPal-Mock-Response", "{\"mock_application_codes\" : \"INSTRUMENT_DECLINED\"}");
            request.RequestBody(new OrderActionRequest());
            await PaypalClient.GetClient().Execute(request);
        }
    }
}