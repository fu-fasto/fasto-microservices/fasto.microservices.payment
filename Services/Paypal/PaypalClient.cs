﻿using PayPalCheckoutSdk.Core;

namespace FastO.Microservices.Payment.Services.Paypal
{
    public class PaypalClient
    {
        private static string clientId =
            "AdCmIZe5D7zXWTtEjOIGxBY2S7YfK88JhXPBVHhrdoaMgCmeE77AcjL4klRU7Vjwzi_Z56cImD17f1u6";

        private static string secret =
            "EIQDHye2y4qBK0XtICRHMrzY39jaPN1aMfusrHqfGbTE6NpYVC-vmMWJafH8s_vBLtghwb7TWZ3uu6jK";
        
        private static PayPalEnvironment GetEnvironment()
        {
            return new SandboxEnvironment(clientId, secret);
        }
        
        public static PayPalHttpClient GetClient()
        {
            return new(GetEnvironment());
        }
        
        public static PayPalHttpClient GetClient(string refreshToken)
        {
            return new(GetEnvironment(), refreshToken);
        }
    }
}