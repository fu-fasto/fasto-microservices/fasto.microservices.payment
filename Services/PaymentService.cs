﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;
using FastO.Microservices.Payment.DataModels;
using FastO.Microservices.Payment.DataModels.TransactionData;
using FastO.Microservices.Payment.Services.Momo;
using FastO.Microservices.Payment.Services.Paypal;
using PayPalCheckoutSdk.Orders;

namespace FastO.Microservices.Payment.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly MomoService _momoService;

        public PaymentService(MomoService momoService)
        {
            _momoService = momoService;
        }

        public Task CreatePaymentTransaction(CreatePaymentTransaction command, CancellationToken cancellationToken)
        {
            switch (command.Provider.ToUpper())
            {
                case Provider.PayPal:
                {
                    return CreatePaypalTransaction(command, cancellationToken);
                }
                
                case Provider.Momo:
                {
                    return _momoService.CreateMomoPaymentTransaction(command, cancellationToken);
                }
                
                default:
                    throw new ArgumentException($"{command.Provider} is not supported!");
            }
        }

        private async Task CreatePaypalTransaction(CreatePaymentTransaction command, CancellationToken cancellationToken)
        {
            // Convert VND to USD
            command.Currency = "USD";
            command.Price /= 23000;
            
            var orderRequest = PaypalService.BuildOrderRequest(command);
            var response = await PaypalService.CreateOrder(orderRequest);
            //Add try-catch later
            var order = response.Result<Order>();
            
            command.TransactionData = new PaypalTransactionData()
            {
                PaypalOrderId = order.Id
            };
        }
    }
}