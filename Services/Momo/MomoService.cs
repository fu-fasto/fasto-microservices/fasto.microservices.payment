﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;
using FastO.Microservices.Payment.DataModels.TransactionData;
using Newtonsoft.Json.Linq;

namespace FastO.Microservices.Payment.Services.Momo
{
    public class MomoService
    {
        private readonly PaymentConfiguration _paymentConfiguration;

        public MomoService(PaymentConfiguration paymentConfiguration)
        {
            _paymentConfiguration = paymentConfiguration;
        }

        public Task CreateMomoPaymentTransaction(CreatePaymentTransaction command,
            CancellationToken cancellationToken)
        {
            // Hard code
            string endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
            string partnerCode = "MOMO5RGX20191128";
            string accessKey = "M8brj9K6E22vXoDB";
            string secretKey = "nqQiVSgDMy809JoPF6OzP5OdBUB550Y4";
            string orderInfo = "Thanh toán hoá hơn";
            string returnUrl = $"{_paymentConfiguration.ReturnHost}/visits";
            string notifyUrl = $"{_paymentConfiguration.NotifyHost}/payment-transactions/complete/{command.Id}";

            string amount = ((int) Math.Round(command.Price, MidpointRounding.ToEven)).ToString();
            string orderId = Guid.NewGuid().ToString();
            string requestId = Guid.NewGuid().ToString();
            string extraData = "";

            //Before sign HMAC SHA256 signature
            string rawHash = "partnerCode=" +
                             partnerCode + "&accessKey=" +
                             accessKey + "&requestId=" +
                             requestId + "&amount=" +
                             amount + "&orderId=" +
                             orderId + "&orderInfo=" +
                             orderInfo + "&returnUrl=" +
                             returnUrl + "&notifyUrl=" +
                             notifyUrl + "&extraData=" +
                             extraData;


            MoMoSecurity crypto = new MoMoSecurity();

            string signature = crypto.SignSha256(rawHash, secretKey);

            //build body json request
            JObject message = new JObject
            {
                {"partnerCode", partnerCode},
                {"accessKey", accessKey},
                {"requestId", requestId},
                {"amount", amount},
                {"orderId", orderId},
                {"orderInfo", orderInfo},
                {"returnUrl", returnUrl},
                {"notifyUrl", notifyUrl},
                {"extraData", extraData},
                {"requestType", "captureMoMoWallet"},
                {"signature", signature}
            };

            string responseFromMomo = PaymentRequest.SendPaymentRequest(endpoint, message.ToString());
            var jObject = JObject.Parse(responseFromMomo);
            command.TransactionData = new MomoTransactionData()
            {
                PayUrl = jObject.GetValue("payUrl").ToString(),
                MomoOrderId = orderId
            };

            return Task.CompletedTask;
        }
    }

    class MoMoSecurity
    {
        private static RNGCryptoServiceProvider _rngCsp = new RNGCryptoServiceProvider();

        public string GetHash(string partnerCode, string merchantRefId,
            string amount, string paymentCode, string storeId, string storeName, string publicKeyXML)
        {
            string json = "{\"partnerCode\":\"" +
                          partnerCode + "\",\"partnerRefId\":\"" +
                          merchantRefId + "\",\"amount\":" +
                          amount + ",\"paymentCode\":\"" +
                          paymentCode + "\",\"storeId\":\"" +
                          storeId + "\",\"storeName\":\"" +
                          storeName + "\"}";

            byte[] data = Encoding.UTF8.GetBytes(json);
            string result = null;
            using (var rsa = new RSACryptoServiceProvider(4096)) //KeySize
            {
                try
                {
                    // MoMo's public key has format PEM.
                    // You must convert it to XML format. Recommend tool: https://superdry.apphb.com/tools/online-rsa-key-converter
                    rsa.FromXmlString(publicKeyXML);
                    var encryptedData = rsa.Encrypt(data, false);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    result = base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }

            return result;
        }

        public string BuildQueryHash(string partnerCode, string merchantRefId,
            string requestId, string publicKey)
        {
            string json = "{\"partnerCode\":\"" +
                          partnerCode + "\",\"partnerRefId\":\"" +
                          merchantRefId + "\",\"requestId\":\"" +
                          requestId + "\"}";

            byte[] data = Encoding.UTF8.GetBytes(json);
            string result;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                try
                {
                    // client encrypting data with public key issued by server
                    rsa.FromXmlString(publicKey);
                    var encryptedData = rsa.Encrypt(data, false);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    result = base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }

            return result;
        }

        public string BuildRefundHash(string partnerCode, string merchantRefId,
            string momoTranId, long amount, string description, string publicKey)
        {
            string json = "{\"partnerCode\":\"" +
                          partnerCode + "\",\"partnerRefId\":\"" +
                          merchantRefId + "\",\"momoTransId\":\"" +
                          momoTranId + "\",\"amount\":" +
                          amount + ",\"description\":\"" +
                          description + "\"}";

            byte[] data = Encoding.UTF8.GetBytes(json);
            string result;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                try
                {
                    // client encrypting data with public key issued by server
                    rsa.FromXmlString(publicKey);
                    var encryptedData = rsa.Encrypt(data, false);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    result = base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }

            return result;
        }

        public string SignSha256(string message, string key)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(key);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            using (var hash256 = new HMACSHA256(keyByte))
            {
                byte[] hashMessage = hash256.ComputeHash(messageBytes);
                string hex = BitConverter.ToString(hashMessage);
                hex = hex.Replace("-", "").ToLower();
                return hex;
            }
        }
    }

    class PaymentRequest
    {
        public static string SendPaymentRequest(string endpoint, string postJsonString)
        {
            try
            {
                var httpWReq = (HttpWebRequest) WebRequest.Create(endpoint);

                var postData = postJsonString;

                var data = Encoding.UTF8.GetBytes(postData);

                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";

                httpWReq.ContentLength = data.Length;
                httpWReq.ReadWriteTimeout = 30000;
                httpWReq.Timeout = 15000;
                Stream stream = httpWReq.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();

                HttpWebResponse response = (HttpWebResponse) httpWReq.GetResponse();

                string jsonResponse = "";

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string temp = null;
                    while ((temp = reader.ReadLine()) != null)
                    {
                        jsonResponse += temp;
                    }
                }

                return jsonResponse;
            }
            catch (WebException e)
            {
                return e.Message;
            }
        }
    }
}