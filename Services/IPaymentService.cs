﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;

namespace FastO.Microservices.Payment.Services
{
    public interface IPaymentService
    {
        Task CreatePaymentTransaction(CreatePaymentTransaction command, CancellationToken cancellationToken = default);
    }
}