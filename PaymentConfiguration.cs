﻿namespace FastO.Microservices.Payment
{
    public class PaymentConfiguration
    {
        public string NotifyHost { get; set; }
        public string ReturnHost { get; set; }
    }
}