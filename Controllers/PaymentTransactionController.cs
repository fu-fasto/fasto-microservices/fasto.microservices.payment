﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;
using FastO.Microservices.Payment.Dtos;
using FastO.Microservices.Payment.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Payment.Controllers
{
    [ApiController]
    [Route("payment-transactions")]
    public class PaymentTransactionController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public PaymentTransactionController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPaymentTransactions([FromQuery] GetAllPaymenTransactions query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPaymentTransaction([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOnePaymentTransaction {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentTransaction([FromBody] CreatePaymentTransaction command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            var createPaymentTransactionResponse = new CreatePaymentTransactionResponse
            {
                Id = command.Id,
                TransactionData = command.TransactionData
            };

            return Accepted(createPaymentTransactionResponse);
        }

        [HttpPost("complete/{id}")]
        public async Task<IActionResult> CompletePaymentTransaction([FromRoute] Guid id)
        {
            var completePaymentTransaction = new CompletePaymentTransaction()
            {
                PaymentTransactionId = id
            };
            await _commandBus.SendAsync(completePaymentTransaction);

            return Ok();
        }
    }
}