﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Payment.Commands;
using FastO.Microservices.Payment.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Payment.Controllers
{
    [ApiController]
    [Route("payment-accounts")]
    public class PaymentAccountController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public PaymentAccountController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOnePaymentAccount([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOnePaymentAccount() {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPaymentAccounts([FromQuery] GetAllPaymentAccounts query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentAccount([FromBody] CreatePaymentAccount createPaymentAccount,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(createPaymentAccount, cancellationToken);
            return Accepted(createPaymentAccount.Id);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdatePaymentAccount([FromRoute] Guid id, 
            [FromBody] UpdatePaymentAccount updatePaymentAccount,
            CancellationToken cancellationToken)
        {
            updatePaymentAccount.Id = id;
            await _commandBus.SendAsync(updatePaymentAccount, cancellationToken);
            return Accepted(id);
        }
    }
}